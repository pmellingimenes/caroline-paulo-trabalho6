package main.java.friends;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Scanner;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class App {

	public static void main(String[] args) throws UnknownHostException {
		System.out.println("### Bem Vindo ao verificador de episódios do friends");
		System.out.println("Qual a temporada que deseja consultar: ");
		Scanner in = new Scanner(System.in);
		String season = in.nextLine();

		MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));
		DB database = mongoClient.getDB("friends");
		DBCollection collection = database.getCollection("samples_friends");
		DBCursor cursor = collection.find(new BasicDBObject("season", Integer.parseInt(season.toString())));

		List<DBObject> episodes = cursor.toArray();

		for (DBObject episode : episodes) {
			System.out.println((String) episode.get("name"));
		}

		in.close();
	}
}
